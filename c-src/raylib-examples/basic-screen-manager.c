#include <stdio.h>
#include "raylib.h"

typedef enum GameScreen {LOGO = 0, TITLE, GAMEPLAY, ENDING} GameScreen;

int main(void){

  /* const int screenWidth = 800; */
  /* const int screenHeight = 450; */
  const int screenWidth = 1920;
  const int screenHeight = 1080;

  InitWindow(screenWidth, screenHeight, "raylib [core] example - window");

  GameScreen currentScreen = LOGO;

  int framesCounter = 0;

  SetTargetFPS(60);

  while(!WindowShouldClose()){
    switch(currentScreen){
    case LOGO:
      {
	framesCounter++;
	
	if (framesCounter > 120){
	  currentScreen = TITLE;
	}
      } break;
    case TITLE:
      {
	if (IsKeyPressed(KEY_ENTER) || IsGestureDetected(GESTURE_TAP))
	  {
	    currentScreen = GAMEPLAY;
	  }
      }break;
   case GAMEPLAY:
     {
       if (IsKeyPressed(KEY_ENTER) || IsGestureDetected(GESTURE_TAP))
	 {
	   currentScreen = ENDING;
	 }
       }break;
    case ENDING:
      {
	if (IsKeyPressed(KEY_ENTER) || IsGestureDetected(GESTURE_TAP))
	 {
	   currentScreen = TITLE;
	 }
       }break;
      default: break;
    }

    BeginDrawing();
    
    ClearBackground(RAYWHITE);    //ClearBackground(RAYWHITE);
    switch(currentScreen){
      case LOGO:
	{
	  DrawText("LOGO SCREEN",20,20,40,LIGHTGRAY);
	  if (framesCounter < 60) {
	    DrawText("WAIT for 2 SECONDS...",290,290,20,GRAY);
	  } 
	  else if (framesCounter > 60 && framesCounter < 120) {
	    DrawText("WAIT for 1 SECONDS...",290,290,20,GRAY);
	  } 

	} break;
      case TITLE:
	{
	  DrawRectangle(0,0,screenWidth,screenHeight,GREEN);
	  DrawText("TITLE SCREEN",20,20,40,LIGHTGRAY);
	  DrawText("PRESS ENTER or TAP to jump to GAMEPLAY SCREEN",290,290,20,DARKGREEN);
	} break;
      case GAMEPLAY:
	{
	  DrawRectangle(0,0,screenWidth,screenHeight,PURPLE);
	  DrawText("GAMEPLAY SCREEN",20,20,40,LIGHTGRAY);
	  DrawText("PRESS ENTER or TAP to jump to GAMEPLAY SCREEN",290,290,20,MAROON);
	} break;
      case ENDING:
	{
	  DrawRectangle(0,0,screenWidth,screenHeight,BLUE);
	  DrawText("ENDING SCREEN",20,20,40,DARKBLUE);
	  DrawText("PRESS ENTER or TAP to jump to GAMEPLAY SCREEN",290,290,20,DARKBLUE);
	} break;
	default: break;
    }
    EndDrawing();
  }

  CloseWindow();

  return 0;
}
