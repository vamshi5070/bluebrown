#include <iostream>
#include <raylib.h>

using namespace std;

int main(){
  const int screenWidth = 1280;
  const int screenHeight = 800;
  // cout << "Emacs is great" << endl;
  const int paddleHeight = 120;
  const int paddleWidth = 25;
  const int ballDiameter = 20;
  InitWindow(screenWidth,screenHeight,"Pong game");
  SetTargetFPS(60);
  
  while(WindowShouldClose() == false){
    BeginDrawing();

    DrawLine(screenWidth/2,0,screenWidth/2,screenHeight,WHITE);
    DrawCircle(screenWidth/2,screenHeight/2,ballDiameter,WHITE);
    
    DrawRectangle(0,screenHeight/2-(paddleHeight/2),paddleWidth,paddleHeight,WHITE);      
    DrawRectangle(screenWidth-paddleWidth,screenHeight/2-(paddleHeight/2),paddleWidth,paddleHeight,WHITE);

    EndDrawing();
  }
  
  CloseWindow();
  return 0;
}
