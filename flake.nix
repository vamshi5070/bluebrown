{
  inputs = { nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable"; };
  outputs = { self, nixpkgs }:
    let
      c-buildPhase = ''
                  	gcc -Wall -Wextra -std=c11 `pkg-config --cflags raylib` -o hello ./c-src/main.c  `pkg-config --libs raylib`
        	          '';
      cpp-buildPhase = ''
                  	g++ -Wall -Wextra -std=c11 `pkg-config --cflags raylib` -o hello ./cpp-src/main.cpp  `pkg-config --libs raylib`
        	          '';
    in {
      defaultPackage.aarch64-linux =
        with import nixpkgs { system = "aarch64-linux"; };
        stdenv.mkDerivation {
          name = "hello";
          src = self;
          buildInputs = [ gcc ];
          buildPhase = cpp-buildPhase;
          nativeBuildInputs = [ pkgs.pkg-config pkgs.raylib ];
          installPhase = ''
            	             mkdir -p $out/bin
                           install -t $out/bin hello
          '';
        };
    };
}
